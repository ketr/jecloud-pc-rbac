import { h, ref } from 'vue';
import AasaRoleAuthorization from '../views/saas-role-authorization/layout.vue';
import MatrixCopyView from '../views/matrix/copy/index.vue';
import { Modal } from '@jecloud/ui';
import { getBodyHeight } from '@jecloud/utils';

export function showSaasRbacWin({ productData }, fn) {
  Modal.window({
    title: '角色权限',
    width: '100%',
    height: '100%',
    bodyStyle: 'padding: 0 10px 10px 10px;',
    headerStyle: 'height:50px;',
    content() {
      return h(AasaRoleAuthorization, { productData });
    },
    //关闭方法
    onClose(model) {
      fn && fn();
    },
  });
}

export function showMatrixCopyWin(options, fn) {
  const $plugin = ref();
  const pluginSlot = () => {
    return <MatrixCopyView ref={$plugin} options={options} fn={fn}></MatrixCopyView>;
  };
  // 确定按钮
  const onOkButtonClick = (obj) => {
    $plugin.value.doSave(obj);
  };
  Modal.window({
    title: '复制矩阵字段',
    width: '800px',
    height: getBodyHeight() - 150,
    bodyStyle: 'padding: 0 10px 10px 10px;',
    headerStyle: 'height:50px;',
    content: () => pluginSlot(),
    okButton: (obj) => {
      onOkButtonClick(obj);
      return false;
    },
    cancelButton: true,
  });
}

// 校验规则
export const verifyData = {
  code: [
    { required: true, message: '该输入项为必填项' },
    { max: 40, message: '不能大于40个字符' },
    {
      pattern: '^[A-Z]{1}[A-Z_0-9]{0,100}$',
      message: '编码由大写字母、下划线、数字组成,且首位为大写字母',
    },
  ],
  title: [
    {
      required: true,
      message: '该输入项为必填项',
    },
    {
      max: 40,
      message: '不能大于40个字符',
    },
    {
      pattern: '^[^\\\\/*?:"\'<>|\x22]+$',
      message: '不能输入非法字符',
    },
  ],
  required: [
    {
      required: true,
      message: '该输入项为必填项',
    },
  ],
};
